﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Noise : MonoBehaviour {
    [SerializeField]
    int depth = 20;
    [SerializeField]
    int width = 256;
    [SerializeField]
    int height = 256;
    [SerializeField]
    float scale = 10f;

	// Use this for initialization
	void Start () {

        

	}
	
    TerrainData GenerateTerrain(TerrainData data)
    {
        data.heightmapResolution = width+1;

        data.size = new Vector3(width,depth,height);

        data.SetHeights(0, 0, GenerateHeight());
        return data;
    }
    float[,] GenerateHeight()
    {
        float[,] heights = new float[width, height];
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                heights[x, y] = CalculateHeight(x, y);
            }
        }

        return heights;
    }

    float CalculateHeight(int x, int y)
    {
        float xCord = (float)x / width * scale;
        float yCord = (float)y / height * scale;

        return Mathf.PerlinNoise(xCord, yCord);
    }


	// Update is called once per frame
	void Update () {
        Terrain terrain = GetComponent<Terrain>();
        terrain.terrainData = GenerateTerrain(terrain.terrainData);
    }
}
