﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum TreeDensity { Scarse = 12, Low = 9, Medium = 7, Heigh = 4, Plentiful = 3 }
public static class NoiseGenerator {

    public enum NormalizeMode {Local, Global }
    
    public static float[,] GenerateNoiseMap(int mapWidth, int mapHeight,int seed, float scale, int octaves, float persistance, float lacunarity, Vector2 offset, NormalizeMode normalizeMode)
    {
        float[,] noiseMap = new float[mapWidth,mapHeight];

        System.Random prng = new System.Random(seed);
        Vector2[] octaveOffsets = new Vector2[octaves];

        float maxPossibleHeight = 0;
        float amplitude = 1;
        float frequency = 1;

        for (int i = 0; i < octaves; i++)
        {
            float offsetX = prng.Next(-100000, 100000) + offset.x;
            float offsetY = prng.Next(-100000, 100000) - offset.y;
            octaveOffsets[i] = new Vector2(offsetX,offsetY);

            maxPossibleHeight += amplitude;
            amplitude *= persistance;
        }


        if (scale <= 0)
            scale = 0.00001f;

        float maxLocalNoiseHeight = float.MinValue;
        float minLocalNoiseHeight = float.MaxValue;

        float halfWidth = mapWidth / 2f;
        float halfHeight = mapHeight / 2f;

        for (int y = 0; y < mapHeight; y++)
        {
            for (int x = 0; x < mapWidth; x++)
            {

                amplitude = 1;
                frequency = 1;
                float noiseHeight = 0;

                for (int i = 0; i < octaves; i++)
                {
                    float sampleX = (x-halfWidth + octaveOffsets[i].x) / scale * frequency ;
                    float sampleY = (y-halfHeight + octaveOffsets[i].y) / scale * frequency ;

                    float perlinValue = Mathf.PerlinNoise(sampleX, sampleY) * 2 - 1;
                    noiseMap[x, y] = perlinValue;
                    noiseHeight += perlinValue * amplitude;

                    amplitude *= persistance;
                    frequency *= lacunarity;
                }
                if(noiseHeight > maxLocalNoiseHeight)
                {
                    maxLocalNoiseHeight = noiseHeight;
                }
                else if(noiseHeight < minLocalNoiseHeight){
                    minLocalNoiseHeight = noiseHeight;
                }

                noiseMap[x, y] = noiseHeight;

            }
        }
        for (int y = 0; y < mapHeight; y++)
        {
            for (int x = 0; x < mapWidth; x++)
            {
                if (normalizeMode == NormalizeMode.Local)
                {
                    noiseMap[x, y] = Mathf.InverseLerp(minLocalNoiseHeight, maxLocalNoiseHeight, noiseMap[x, y]);
                }
                else
                {
                    float normalizedHeight = (noiseMap[x, y] + 1) / (maxPossibleHeight * 0.95f);
                    noiseMap[x, y] = Mathf.Clamp(normalizedHeight,0,int.MaxValue);
                }
            }
        }

                return noiseMap;
    }

    public static float[,] GenerateBlueNoise(float[,] values, int size, TreeDensity density)
    {
        int trees = 0;
        float[,] treeMap = new float[size, size];
        for (int yc = 0; yc < size; yc++)
        {
            for (int xc = 0; xc < size; xc++)
            {
                double max = 0;
                for (int yn = yc - (int)density; yn <= yc + (int)density; yn++)
                {
                    for (int xn = xc - (int)density; xn <= xc + (int)density; xn++)
                    {
                        if (xn >= 0 && yn >= 0 && xn <= size - 1 && yn <= size - 1)
                        {
                            double e = values[xn, yn];
                            if (e > max) { max = e; }
                        }
                    }
                }
                if (values[xc, yc] == max)
                {
                    //place dot here lel
                    
                    treeMap[xc, yc] = 0;
                    trees++;
                }
                else
                {
                    treeMap[xc, yc] = 1;
                }
            }
        }
        Debug.Log("placed: " + trees + " trees");
        return treeMap;
    }

    public static float[,] GenerateMoistureMap(int size, int seed, float scale, float persistance, float lacunarity,Vector2 offset, NormalizeMode normalizeMode)
    {
        float[,] moistureHeightMap = GenerateNoiseMap(size,size,seed,scale,3,persistance,lacunarity,offset,NormalizeMode.Global);
        float maxHumidity = float.MinValue;
        float minHumidity = float.MaxValue;
        for (int y = 0; y < size; y++)
        {
            for (int x = 0; x < size; x++)
            {
                if (moistureHeightMap[x,y] > maxHumidity)
                {
                    maxHumidity = moistureHeightMap[x, y];
                }else if (moistureHeightMap[x, y] < minHumidity)
                {
                    minHumidity = moistureHeightMap[x, y];
                }
            }
        }
        for (int y = 0; y < size; y++)
        {
            for (int x = 0; x < size; x++)
            {
                moistureHeightMap[x, y] = Mathf.InverseLerp(minHumidity, maxHumidity, moistureHeightMap[x, y]);
            }
        }
                return moistureHeightMap;
    }
}

 

	

