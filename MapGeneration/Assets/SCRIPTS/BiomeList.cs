﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BiomeList : MonoBehaviour {

    public Biome[] biomes;


    [System.Serializable]
	public struct Biome
    {
        public string name;
        [Range(0,1)]
        public float startHeight;
        [Range(0, 1)]
        public float moistureMax;
        [Range(0, 1)]
        public float moistureMin;
        public Color biomeColor;
    }
}


