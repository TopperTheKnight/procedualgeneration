﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TextureGenerator  {

    public static Texture2D TextureFromColorMap(Color[] colorMap, int width, int height)
    {
        Texture2D texture = new Texture2D(width, height);
        texture.filterMode = FilterMode.Point;
        texture.wrapMode = TextureWrapMode.Clamp;
        texture.SetPixels(colorMap);
        texture.Apply();
        return texture;
    }

    public static Texture2D TextureFromHeightMap(float[,] heightMap)
    {
        int width = heightMap.GetLength(0);
        int height = heightMap.GetLength(1);

        Color[] colorMap = new Color[width * height];
        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                colorMap[y * width + x] = Color.Lerp(Color.black, Color.white, heightMap[x, y]);
            }
        }
        return TextureFromColorMap(colorMap, width, height);
    }

    public static Texture2D TextureFromMoistureMap(float[,] moistureMap)
    {

        Color[] colorMap = new Color[moistureMap.GetLength(0) * moistureMap.GetLength(0)];

        for (int x = 0; x < moistureMap.GetLength(0); x++)
        {
            for (int y = 0; y < moistureMap.GetLength(0); y++)
            {
                colorMap[y * moistureMap.GetLength(0) + x] = Color.Lerp(Color.red, Color.blue, moistureMap[x,y]);
            }
        }
        return TextureFromColorMap(colorMap, moistureMap.GetLength(0), moistureMap.GetLength(0));
    }

}
