﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Threading;


public class MapGenerator : MonoBehaviour
{
    public enum DrawMode { NoiseMap, ColorMap, MoistureMap, TreeMap, Mesh, FalloffMap}
    public DrawMode drawMode;
    public TreeDensity density;

    public NoiseGenerator.NormalizeMode normalikzeMode;

    public const int mapChunkSize = 239;
    [Range(0,6)]
    public int editorPreviewlevelOfDetail;
    public float noiseScale;
    public float moistureMapScale;

    public int octaves;
    [Range(0,1)]
    public float persistance;
    public float lacunatiry;

    public int seed;
    public Vector2 offset;

    public bool useFalloff;

    public float heightMultiplier;
    public AnimationCurve meshHeightCurve;
    public bool autoUpdate;

    public TerrainType[] regions;
    public Biome[] biomes;

    float[,] falloffMap;

    Queue<MapThreadInfo<MapData>> mapDataThreadInfoQueue = new Queue<MapThreadInfo<MapData>>();
    Queue<MapThreadInfo<MeshData>> meshDataThreadInfoQueue = new Queue<MapThreadInfo<MeshData>>();

    private void Awake()
    {
        falloffMap = FalloffGenerator.GenerateFalloffMap(mapChunkSize);
    }
    public void DrawMapInEditor()
    {
        MapData mapData = GenerateMapData(Vector2.zero);
        MapDisplay display = FindObjectOfType<MapDisplay>();
        if (drawMode == DrawMode.NoiseMap)
        {
            display.DrawTexture(TextureGenerator.TextureFromHeightMap(mapData.heightMap));
        }
        else if (drawMode == DrawMode.ColorMap)
        {
            display.DrawTexture(TextureGenerator.TextureFromColorMap(mapData.colorMap, mapChunkSize, mapChunkSize));
        }
        else if (drawMode == DrawMode.Mesh)
        {
            display.DrawMesh(MeshGenerator.GenerateTerrainMesh(mapData.heightMap, heightMultiplier, meshHeightCurve, editorPreviewlevelOfDetail), TextureGenerator.TextureFromColorMap(mapData.colorMap, mapChunkSize, mapChunkSize));
        }else if (drawMode == DrawMode.FalloffMap)
        {
            display.DrawTexture(TextureGenerator.TextureFromHeightMap(FalloffGenerator.GenerateFalloffMap(mapChunkSize)));
        }else if (drawMode == DrawMode.TreeMap)
        {
            float[,] treeHeightMap = NoiseGenerator.GenerateNoiseMap(mapChunkSize+2,mapChunkSize+2,1,2,1,0,0,Vector2.zero,NoiseGenerator.NormalizeMode.Global);
            display.DrawTexture(TextureGenerator.TextureFromHeightMap(NoiseGenerator.GenerateBlueNoise(treeHeightMap,mapChunkSize,density)));
        }else if (drawMode == DrawMode.MoistureMap)
        {
            float[,] moistureMap = NoiseGenerator.GenerateMoistureMap(mapChunkSize + 2, seed, noiseScale, persistance, lacunatiry,offset, normalikzeMode);
            display.DrawTexture(TextureGenerator.TextureFromMoistureMap(moistureMap));
        }
    }

    public void RequestMapData(Vector2 centre ,Action<MapData> callback)
    {
        ThreadStart threadStart = delegate
        {
            MapDataThread(centre, callback);
        };
        new Thread(threadStart).Start();
    }

    public void RequestMeshData(MapData mapData,int lod, Action<MeshData> callback)
    {
        ThreadStart threadStart = delegate {
            MeshDataThread(mapData, lod, callback);
        };
        new Thread(threadStart).Start();
    }

    void MeshDataThread(MapData mapData, int lod, Action<MeshData> callback)
    {
        MeshData meshData = MeshGenerator.GenerateTerrainMesh(mapData.heightMap, heightMultiplier, meshHeightCurve, lod);
        lock (meshDataThreadInfoQueue)
        {
            meshDataThreadInfoQueue.Enqueue(new MapThreadInfo<MeshData>(callback, meshData));
        }

    }
    void MapDataThread(Vector2 centre,Action<MapData> callback)
    {
        MapData mapData = GenerateMapData(centre);
        lock (mapDataThreadInfoQueue)
        {
            mapDataThreadInfoQueue.Enqueue(new MapThreadInfo<MapData>(callback, mapData));
        }
    }

    private void Update()
    {
        if (mapDataThreadInfoQueue.Count > 0)
        {
            for (int i = 0; i < mapDataThreadInfoQueue.Count; i++)
            {
                MapThreadInfo<MapData> threadInfo = mapDataThreadInfoQueue.Dequeue();
                threadInfo.callback(threadInfo.parameter);
            }
        }
        if(meshDataThreadInfoQueue.Count > 0)
        {
            for (int i = 0; i < meshDataThreadInfoQueue.Count; i++)
            {
                MapThreadInfo<MeshData> threadInfo = meshDataThreadInfoQueue.Dequeue();
                threadInfo.callback(threadInfo.parameter);
            }
        }
    }

    MapData GenerateMapData(Vector2 centre)
    {
        
        float[,] noiseMap = NoiseGenerator.GenerateNoiseMap(mapChunkSize + 2, mapChunkSize + 2, seed, noiseScale, octaves, persistance, lacunatiry,centre + offset, normalikzeMode);
        int tempSeed = (int)(seed * persistance * lacunatiry * 15);
        float tempScale = noiseScale;
        float[,] moistureMap = NoiseGenerator.GenerateMoistureMap(mapChunkSize + 2, tempSeed, tempScale, persistance, lacunatiry, centre + offset , normalikzeMode);
        Color[] colorMap = new Color[mapChunkSize * mapChunkSize];
        
        for (int y = 0; y < mapChunkSize; y++)
        {
            for (int x = 0; x < mapChunkSize; x++)
            {
                if (useFalloff)
                {
                    noiseMap[x, y] = Mathf.Clamp( noiseMap[x, y] - falloffMap[x, y], 0,1);
                }
                float currentHeight = noiseMap[x, y];
                float currentHumidity = moistureMap[x, y];
                if (currentHumidity > 1 || currentHumidity < 0)
                {
                    Debug.Log("Humidity out of range 0-1 : " + currentHumidity);
                }
                for (int i = 0; i < biomes.Length; i++)
                {
                    if (currentHeight >= biomes[i].startHeight && currentHumidity >= biomes[i].moistureMin && currentHumidity <= biomes[i].moistureMax)
                    {
                        colorMap[y * mapChunkSize + x] = biomes[i].biomeColor;
                        
                    } else if(biomes.Length > i + 1)
                    {
                    }else
                    {
                        break;
                    }
                }
            }
        }
        return new MapData(noiseMap,colorMap,moistureMap);
        
    }

    private void OnValidate()
    {
        
        if (lacunatiry < 1)
        {
            lacunatiry = 1;
        }
        if (octaves<0)
        {
            octaves = 0;
        }
        falloffMap = FalloffGenerator.GenerateFalloffMap(mapChunkSize);
    }

    struct MapThreadInfo<T>
    {
        public readonly Action<T> callback;
        public readonly T parameter;
        public MapThreadInfo(Action<T> callback, T parameter)
        {
            this.callback = callback;
            this.parameter = parameter;
        }
    }
}

[System.Serializable]
public struct TerrainType
{
    public string name;
    public float height;
    public Color color;
}

[System.Serializable]
public struct Biome
{
    public string name;
    [Range(0, 1)]
    public float startHeight;
    [Range(0, 1)]
    public float moistureMax;
    [Range(0, 1)]
    public float moistureMin;
    public Color biomeColor;
}

public struct MapData
{
    public readonly float[,] heightMap;
    public readonly float[,] moistureMap;
    public readonly Color[] colorMap;
    public MapData(float[,] heightMap, Color[] colorMap, float[,] moistureMap)
    {
        this.moistureMap = moistureMap;
        this.heightMap = heightMap;
        this.colorMap = colorMap;
    }
}
