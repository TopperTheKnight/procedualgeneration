﻿
using UnityEngine;
using System.Collections;
using UnityEditor;
[CustomEditor (typeof(MapGenerator))]
public class NewBehaviourScript : Editor {

    public override void OnInspectorGUI()
    {
        MapGenerator mapGen = (MapGenerator)target;

        if (DrawDefaultInspector())
        {
            if (mapGen.autoUpdate)
            {
                mapGen.DrawMapInEditor();
            }
        }

        DrawDefaultInspector();
        if (GUILayout.Button("Generate"))
        {
            Debug.Log("Calling draw map");
            mapGen.DrawMapInEditor();
        }
    }
}
